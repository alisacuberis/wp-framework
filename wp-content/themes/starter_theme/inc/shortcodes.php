<?php
/**
 * Custom Shortcodes
 *
 * @package Cuberis
 */

// Buttons
function buttons( $atts, $content = null ) {
	extract( shortcode_atts( array(
	'type' => 'radius', /* radius, round */
	'size' => 'medium', /* small, medium, large */
	'color' => 'blue',
	'nice' => 'false',
	'url'  => '',
	'text' => '', 
	), $atts ) );
	
	$output = '<a href="' . $url . '" class="button '. $type . ' ' . $size . ' ' . $color;
	if( $nice == 'true' ){ $output .= ' nice';}
	$output .= '">';
	$output .= $text;
	$output .= '</a>';
	
	return $output;
}

add_shortcode('button', 'buttons'); 

// Alerts
function alerts( $atts, $content = null ) {
	extract( shortcode_atts( array(
	'type' => '	', /* warning, success, error */
	'close' => 'false', /* display close link */
	'text' => '', 
	), $atts ) );
	
	$output = '<div class="fade in alert-box '. $type . '">';
	
	$output .= $text;
	if($close == 'true') {
		$output .= '<a class="close" href="#">×</a></div>';
	}
	
	return $output;
}

add_shortcode('alert', 'alerts');

// Panels
function panels( $atts, $content = null ) {
	extract( shortcode_atts( array(
	'type' => '	', /* warning, success, error */
	'close' => 'false', /* display close link */
	'text' => '', 
	), $atts ) );
	
	$output = '<div class="panel">';
	$output .= $text;
	$output .= '</div>';
	
	return $output;
}

add_shortcode('panel', 'panels');

// Shortcode for columns
function columns_shortcode($atts, $content = null) {
    // remove orphan </p> from beginning of shortcode content
    if (substr($content, 0, 4) == '</p>') $content = substr($content, 4);
    // remove orphan <p> from end of shortcode content
    if (substr($content, -3, 3) == '<p>') $content = substr($content, 0, -3);

    return '<div class="text-columns">'.$content.'</div>';
}
add_shortcode('columns', 'columns_shortcode');

// Shortcode for collapsible sections in content
function collapse_shortcode($atts, $content = null) {
    return '<div class="collapsible">'.$content.'</div>';
}
add_shortcode('collapse', 'collapse_shortcode');

?>