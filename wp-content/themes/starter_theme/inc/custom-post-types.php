<?php
/**
 *
 * @package Cuberis
 */

function custom_post_types() { 
	register_post_type( 'custom_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		array('labels' => array(
			'name' => __('Custom Types', 'cuberis'),
			'singular_name' => __('Custom Type', 'cuberis'),
			'add_new' => __('Add New', 'cuberis'),
			'add_new_item' => __('Add New Custom Type', 'cuberis'),
			'edit' => __( 'Edit' , 'cuberis'),
			'edit_item' => __('Edit Custom Types', 'cuberis'),
			'new_item' => __('New Custom Type', 'cuberis'),
			'view_item' => __('View Custom Type', 'cuberis'),
			'search_items' => __('Search Custom Type', 'cuberis'),
			'not_found' =>  __('Nothing found in the Database.', 'cuberis'),
			'not_found_in_trash' => __('Nothing found in Trash', 'cuberis'),
			'parent_item_colon' => ''
			),
			'public' => true,
			'exclude_from_search' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => false,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			//'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky'),
			'has_archive' => false,
			'rewrite' => true,
			'query_var' => true
	 	)
	);
} 

// adding the function to the Wordpress init
//add_action( 'init', 'custom_post_types');
	
/*
for more information on taxonomies, go here:
http://codex.wordpress.org/Function_Reference/register_taxonomy
*/
	
register_taxonomy( 'custom_cat', 
	//array('custom_type'), /* which post type is this taxonomy attached to? */
	array('hierarchical' => true,     /* if this is true it acts like categories / false it ascts like tags */             
		'labels' => array(
			'name' => __( 'Custom Categories', 'cuberis' ),
			'singular_name' => __( 'Custom Category', 'cuberis' ),
			'search_items' =>  __( 'Search Custom Categories', 'cuberis' ),
			'all_items' => __( 'All Custom Categories', 'cuberis' ),
			'parent_item' => __( 'Parent Custom Category', 'cuberis' ),
			'parent_item_colon' => __( 'Parent Custom Category:', 'cuberis' ),
			'edit_item' => __( 'Edit Custom Category', 'cuberis' ),
			'update_item' => __( 'Update Custom Category', 'cuberis' ),
			'add_new_item' => __( 'Add New Custom Category', 'cuberis' ),
			'new_item_name' => __( 'New Custom Category Name', 'cuberis' )
		),
		'show_ui' => true,
		'query_var' => true,
	)
);   

?>