<?php
/**
 * Cuberis functions and definitions
 *
 * @package Cuberis
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'cuberis_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function cuberis_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Cuberis, use a find and replace
	 * to change 'cuberis' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'cuberis', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'cuberis' ),
	) );

	// Enable support for Post Formats.
	//add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	// Setup the WordPress core custom background feature.
	/*add_theme_support( 'custom-background', apply_filters( 'cuberis_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );*/

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
		'caption',
	) );
}
endif; // cuberis_setup
add_action( 'after_setup_theme', 'cuberis_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function cuberis_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'cuberis' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'cuberis_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function cuberis_scripts() {
    // stylesheets
	wp_enqueue_style( 'cuberis-style', get_stylesheet_uri() );
    wp_enqueue_style( 'jquery-ui', get_template_directory_uri() . '/stylesheets/jquery-ui/jquery-ui-1.9.2.custom.css' );
    wp_enqueue_style( 'foundation-app', get_template_directory_uri() . '/stylesheets/app.css' );

    // _s scripts
	wp_enqueue_script( 'cuberis-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

    // foundation scripts
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/bower_components/modernizr/modernizr.js' );
	wp_enqueue_script( 'foundation', get_template_directory_uri(). '/bower_components/foundation/js/foundation.min.js', array('jquery'), '', true);

    // optional helpful scripts
    wp_enqueue_script( 'skrollr', get_template_directory_uri(). '/js/skrollr.min.js', array('jquery'), '0.6.24', true );
    wp_enqueue_script( 'waypoints', get_template_directory_uri(). '/js/waypoints.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'waypoints-sticky', get_template_directory_uri(). '/js/waypoints-sticky.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'stellar', get_template_directory_uri() . '/js/jquery.stellar.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'hoverintent', get_template_directory_uri() . '/js/jquery-hoverIntent.min.js', array('jquery'), 'r6', true );
    wp_enqueue_script( 'smart-resize', get_template_directory_uri() . '/js/jquery.smartresize.js', array('jquery'), '', true );

    // custom scripts
    wp_enqueue_script( 'custom-scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'cuberis_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
//require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Add custom shortcodes
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 *  Add custom post types
 */
require get_template_directory() . '/inc/custom-post-types.php';

/*********** misc improvements ************/

// Enable shortcodes in widgets
add_filter('widget_text', 'do_shortcode');

// Remove height/width attributes on images so they can be responsive
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );

function remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

// Adds styles to TinyMCE
function add_tiny_mce_before_init( $init_array ) {
    //$init_array['theme_advanced_styles'] = "Main Heading=h2;Sub Heading=h3;Button=button";
    $style_formats = array(
        array(
            'title' => 'Main Heading',
            'block' => 'h2'
        ), 
        array(
            'title' => 'Sub Heading',
            'block' => 'h3'
        ),
        array(
            'title' => 'Plain Text',
            'block' => 'p'
        )
    );
    $init_array['style_formats'] = json_encode($style_formats);
    return $init_array;
}
add_filter( 'tiny_mce_before_init', 'add_tiny_mce_before_init' );

// Load CSS to TinyMCE editor
function add_mce_css( $mce_css ) {
    if ( ! empty( $mce_css ) )
        $mce_css .= ',';

    $mce_css .= get_template_directory_uri() . '/stylesheets/editor.css';

    return $mce_css;
}
add_filter( 'mce_css', 'add_mce_css' );

// Customize the WordPress Footer
function remove_footer_admin () {
    echo '&copy; Cuberis '.date('Y').'. All rights reserved.';
}
add_filter('admin_footer_text', 'remove_footer_admin');


// Custom link for login logo
function wpc_url_login(){
    return home_url();
}
add_filter('login_headerurl', 'wpc_url_login');

// Custom WordPress Login Logo
function login_css() {
    //wp_enqueue_style( 'login_css', get_template_directory_uri() . '/stylesheets/login.css' );
}
add_action('login_head', 'login_css');

// Add a Cuberis widget in the Dashboard
function wpc_dashboard_widget_function() {
    echo "<ul>
    <li>Theme Release: INSERT LAUNCH DATE IN HERE</li>
    <li>Author: Cuberis</li>
    <li>Client: INSERT CLIENT NAME IN HERE</li>
    </ul>";
}
function wpc_add_dashboard_widgets() {
    wp_add_dashboard_widget('wp_dashboard_widget', 'Technical information', 'wpc_dashboard_widget_function');
}
add_action('wp_dashboard_setup', 'wpc_add_dashboard_widgets' );

// Remove unnecessary widgets from the Dashboard
add_action('wp_dashboard_setup', 'wpc_dashboard_widgets');
function wpc_dashboard_widgets() {
    global $wp_meta_boxes;
    // Today widget
    unset($wp_meta_boxes['dashboard']['normal']['high']['dashboard_browser_nag']);
    // Last comments
    //unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    // Recent stuff
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
    // Quickpress
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    // Primary
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
}
